from conans import ConanFile, AutoToolsBuildEnvironment
from conans import tools

class MainAppConan(ConanFile):
    name = "main_app"
    version = "0.2"
    license = "MIT"
    description = "Sum application"
    settings = "os", "compiler", "build_type", "arch"
    url = "https://bitbucket.org/CSahlani/pipelinetestrepo/src/master/"
    exports_sources = "src/*", "build/release/test-results/*", "LICENSE"
    generators = "cmake"

    def build(self):
        with tools.chdir("src"):
            atools = AutoToolsBuildEnvironment(self)
            # atools.configure() # use it to run "./configure" if using autotools
            atools.make()

    def package(self):
        self.copy("*main_app", dst="bin", keep_path=False)
        self.copy("*main_app.exe", dst="bin", keep_path=False)

    def deploy(self):
        self.copy("*", src="bin", dst="bin")
