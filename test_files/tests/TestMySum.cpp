#include <limits.h>
#include <stdint.h>
#include <stdlib.h>
#include "gtest.h"

#ifdef __cplusplus
extern "C" 
{
#endif
#include "my_sum.h"
#ifdef __cplusplus
}
#endif


class TestMySum : public ::testing::Test 
{
 protected:
  virtual void SetUp() 
  {
  }

  virtual void TearDown() 
  {
    // Code here will be called immediately after each test
    // (right before the destructor).
  }
};

TEST_F(TestMySum, positive_numbers)
{
    const int x = 39;
    const int y = 3;
    EXPECT_EQ(42,my_sum(x, y));
}

TEST_F(TestMySum, negative_numbers)
{
    const int x = -63;
    const int y = -7;
    EXPECT_EQ(-70,my_sum(x, y));
}

TEST_F(TestMySum, pos_neg_numbers)
{
    const int x = 1;
    const int y = -81;
    EXPECT_EQ(-80,my_sum(x, y));
}

TEST_F(TestMySum, add_zero)
{
    const int x = 4;
    const int y = 0;
    EXPECT_EQ(4,my_sum(x, y));
}

//This addition gives a number larger than 2^31 -1 by 1 the answer is going to be -2^31
TEST_F(TestMySum, pos_signed_limit)
{
    const int x = 2147483647;
    const int y = 1;
    EXPECT_EQ(-2147483648,my_sum(x, y));
}


//This addition gives a number smaller than -2^31 by -1 the answer is going to be 2^31 -1
TEST_F(TestMySum, neg_signed_limit)
{
    const int x = -2147483648;
    const int y = -1;
    EXPECT_EQ(2147483647,my_sum(x, y));
}

//This addition gives a number larger than 2^32 -1 by 1 the answer is going to be 0
TEST_F(TestMySum, unsigned_limit)
{
    const uint32_t x = 4294967295;
    const uint32_t y = 1;
    EXPECT_EQ(0,my_sum(x, y));
}