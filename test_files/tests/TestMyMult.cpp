#include <limits.h>
#include <stdint.h>
#include <stdlib.h>
#include "gtest.h"

#ifdef __cplusplus
extern "C" 
{
#endif
#include "my_mult.h"
#ifdef __cplusplus
}
#endif


class TestMyMult : public ::testing::Test 
{
 protected:
  virtual void SetUp() 
  {
  }

  virtual void TearDown() 
  {
    // Code here will be called immediately after each test
    // (right before the destructor).
  }
};

TEST_F(TestMyMult, positive_numbers)
{
    const int x = 4;
    const int y = 5;
    EXPECT_EQ(20,my_mult(x, y));
}

TEST_F(TestMyMult, negative_numbers)
{
    const int x = -8;
    const int y = -6;
    EXPECT_EQ(48,my_mult(x, y));
}

TEST_F(TestMyMult, pos_neg_numbers)
{
    const int x = 7;
    const int y = -3;
    EXPECT_EQ(-21,my_mult(x, y));
}

TEST_F(TestMyMult, mult_by_zero)
{
    const int x = 0;
    const int y = 5;
    EXPECT_EQ(0,my_mult(x, y));
}


//This multiplication gives a number larger than 2^31 - 1 by 1 the answer is going to be -2^31
TEST_F(TestMyMult, pos_signed_limit)
{
    const int x = 1073741824;
    const int y = 2;
    EXPECT_EQ(-2147483648,my_mult(x, y));
}


//This multiplication gives a number smaaller than -2^31 by -1 the answer is going to be 2^31 -1
TEST_F(TestMyMult, neg_signed_limit)
{
    const int x = 715827883;
    const int y = -3;
    EXPECT_EQ(2147483647,my_mult(x, y));
}

//This multiplication gives a number larger than 2^32 -1 by 1 the answer is going to be 0
TEST_F(TestMyMult, unsigned_limit)
{
    const uint32_t x = 4294967296;
    const uint32_t y = 1;
    EXPECT_EQ(0,my_mult(x, y));
}
