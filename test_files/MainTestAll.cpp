#include <limits.h>
#include "gtest.h"
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int main(int argc, char **argv) 
{

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}