#include "my_sum.h"
#include "my_mult.h"
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main()
{
    
#ifdef DEBUG
    printf ("\nHello from debug\n");
#endif
#ifdef RELEASE
    printf("\nHello from release\n");
#endif

    int x = 4;
    int y = 5;
// added this comment for the demo

    int z1 = my_sum(x,y);
    printf("\nAddition Result of %d + %d = %d\n", x, y, z1);

    int z2 = my_mult(x,y);
    printf("\nMultiplication Result of %d * %d = %d\n", x, y, z2);

    return 0;
}
